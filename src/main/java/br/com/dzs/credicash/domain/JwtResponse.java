package br.com.dzs.credicash.domain;

import lombok.Getter;

import java.io.Serializable;

/**
 * DZS
 *
 * @author erik_
 * Data Criacao: 17/06/2020 - 20:28
 */
public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -8091879091924046844L;
    @Getter
    private final String jwttoken;

    public JwtResponse(String jwttoken) {
        this.jwttoken = jwttoken;
    }
}
