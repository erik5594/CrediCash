package br.com.dzs.credicash.resources;

import br.com.dzs.credicash.config.jwts.JwtTokenUtil;
import br.com.dzs.credicash.config.security.AppUserDetailsService;
import br.com.dzs.credicash.domain.JwtResponse;
import br.com.dzs.credicash.domain.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

/**
 * DZS
 *
 * @author erik_
 * Data Criacao: 17/06/2020 - 20:20
 */

@RestController
@CrossOrigin
public class JwtAuthenticationResource {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private AppUserDetailsService userDetailsService;

    @RequestMapping(value = "/autenticacao", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody Usuario usuario) throws Exception {
        String username = StringUtils.isBlank(usuario.getUsername()) ? usuario.getEmail():usuario.getUsername();

        authenticate(username, usuario.getSenha());

        final UserDetails userDetails = userDetailsService
                .loadUserByUsername(username);
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
